var canvas 		    = document.getElementById('myCanvas'),
			ctx	   		    = canvas.getContext('2d'),
			x 	   		    = canvas.width/2,
			y 	   		    = canvas.height/2,
			dx	   		    = 4,
			dy 	   		    = -4,
			ballRadius	    = 10,
			paddleHeight    = 10,
			paddleWidth     = 100,
			paddleX		    = (canvas.width - paddleWidth)/2,
			leftPressed     = false,
			rightPressed    = false,
			brickRowCount   = 5,
			brickColumnCount= 11,
			brickWidth	    = 75,
			brickHeight		= 20,
			brickPadding	= 10,
			brickMarginTop	= 30,
			brickMarginLeft	= 30,
			bricks			= [],
			score			= 0,
			dialog			= document.querySelector('dialog'),
			name_input		= document.querySelector('[type=text]'),
			name_button		= document.querySelector('[type=button]'),
			name_input_val,
			ol 				= document.createElement('ol'),
			records,
			leaderboard 	= document.querySelector('.leaderboard'),
			drawing;
		

			function toLocal(){
				records = ol.innerHTML;
				localStorage.setItem('records', records);

			}

			window.addEventListener('load', function(e){
				if (localStorage.getItem('records')){
				ol.innerHTML = localStorage.getItem('records');
				leaderboard.appendChild(ol);
				}
				e.preventDefault();

				dialog.showModal();
					if(ol.childNodes.length > 20){
						localStorage.clear();
					}
			});

			name_button.addEventListener('click', function(){
				name_input_val = name_input.value;
				dialog.close();
			})

			document.getElementById('resume_btn').onclick = function(){
				 drawing = setInterval(draw, 15);
			};
			document.getElementById('pause_btn').onclick = function(){
				clearInterval(drawing);
			};
			
			



			for (var c = 0; c < brickColumnCount; c++){
				bricks[c] = [];
				for(var r = 0; r< brickRowCount; r++){
					bricks[c][r] = { x:0, y:0, status: 1};
				}
			}

			// console.log(bricks);

			function draw (){

				ctx.clearRect(0, 0, canvas.width, canvas.height)
				drawPaddle();
				drawBall();
				drawBricks();
				detection();
				ScoreCounter();


				if (x + dx > canvas.width - ballRadius || x + dx < ballRadius){
					dx = -dx;
				}
				if( y + dy < ballRadius){
					dy = -dy;
				} else if ( y + dy > canvas.height - ballRadius - 15){
					if(x > paddleX && x < paddleX + paddleWidth){
						dy = -dy;
					}
				}
				if( y + dy > canvas.height - ballRadius){
					clearInterval(drawing);
					dialog.innerHTML = `<h3 style = 'margin-top:70px'>Game Over!</h3>`
					dialog.showModal();
					var li = document.createElement('li');
                        	li.innerText = `${name_input_val} - scores:${score}`;
                        	ol.appendChild(li);
                        	leaderboard.appendChild(ol);
                        	toLocal();
					window.onclick = function(){
                        		document.location.reload();
                        	}

				}
				if(rightPressed && paddleX < canvas.width - paddleWidth){
					paddleX += 7;
				}
				if(leftPressed && paddleX > 0){
					paddleX -= 7;
				}
				// requestAnimationFrame(draw);

				
			}

			function keyDownHandler(e){
				if(e.keyCode == 39){
					rightPressed = true;
				}
				else if(e.keyCode == 37){
					leftPressed = true;
				}
			}
			function keyUpHandler(e){
				if(e.keyCode == 39){
					rightPressed = false;
				}
				else if(e.keyCode == 37){
					leftPressed = false;
				}
			}
			document.addEventListener('keydown', keyDownHandler, false);
			document.addEventListener('keyup', keyUpHandler, false);
			document.addEventListener('mousemove', mouseMoveHandler, false)

			function drawBall(){
				ctx.beginPath();
				ctx.arc(x, y, ballRadius, 0, Math.PI*2);
				ctx.fillStyle = 'black';
				ctx.fill();
				ctx.closePath();
				x += dx;
				y += dy;
			}
			function drawPaddle() {
				ctx.beginPath();
				ctx.rect(paddleX, canvas.height-paddleHeight-10, paddleWidth, paddleHeight);
				ctx.fillStyle = 'black';
				ctx.fill();
				ctx.closePath();

			}
			function drawBricks(){
				for(var c=0; c<brickColumnCount; c++){
					for(var r=0; r<brickRowCount; r++){
						if(bricks[c][r].status == 1){
						var brickX	= (c*(brickWidth+brickPadding))+brickMarginLeft,
							brickY	= (r*(brickHeight+brickPadding))+brickMarginTop;
						bricks[c][r].x = brickX;
						bricks[c][r].y = brickY;
						ctx.beginPath();
						ctx.rect(brickX, brickY, brickWidth, brickHeight);
						ctx.fillStyle = 'black';
						ctx.fill();
						ctx.closePath();
						}
					}
				}
			}
			function detection(){
				for(var c=0; c<brickColumnCount; c++){
					for(var r=0; r<brickRowCount; r++){
						var b = bricks[c][r];
						if(b.status == 1){
						if(x > b.x && x < b.x + brickWidth && y > b.y && y < b.y+brickHeight){
							dy = -dy;
							b.status = 0;
							score++;
							if(score == brickRowCount*brickColumnCount) {
                        	dialog.innerHTML = `<h3 style='margin-top:50px'>${name_input_val}, congratulations!</h3>`;
                        	dialog.showModal();
                        	clearInterval(drawing);
                        	var li = document.createElement('li');
                        	li.innerText = `${name_input_val} - scores:${score}`;
                        	ol.appendChild(li)
                        	leaderboard.appendChild(ol);
                        	toLocal();
                        	
                        	window.onclick = function(){
                        		document.location.reload();
                        	}
                        	
                        	// document.location.reload();
                    			}
							}
						}	
					}
				}
			}
			function ScoreCounter(){
				ctx.font = '16px Arial';
				ctx.fillStyle = 'black';
				ctx.fillText('Your Scores: '+score, 8, 20);
			}
			function mouseMoveHandler(e) {
			    var relativeX = e.clientX - canvas.offsetLeft;
			    if(relativeX > 0 && relativeX < canvas.width) {
			        paddleX = relativeX - paddleWidth/2;
			    }
			}


			

		var buttons = document.getElementsByClassName('button');

		for (var i = 0; i < buttons.length; i++) {
		  buttons[i].addEventListener('click', function(e) {
		    var wavePosX = e.offsetX,
		        wavePosY = e.offsetY,
		        button = e.target,
		        buttonWidth = button.clientWidth,
		        wave = document.createElement('span');

		    wave.className = 'material-wave';
		    wave.style.top = wavePosY + 'px';
		    wave.style.left = wavePosX + 'px';
		   
		    
		    e.target.appendChild(wave);

		      
		    setTimeout(function() {
		      wave.style.opacity = 0;
		      wave.style.transform = 'scale(' + buttonWidth / 10 + ')';
		    }, 0);

		    // Remove the wave element after the transition
		    // Should use Modernizr's transitionEndEventName
		    setTimeout(function() {
		      button.removeChild(wave);
		    }, 800);
		  });  
		};